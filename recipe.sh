#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 3 ] || die "usage: revision compiler out_dir "
revision=$1; shift
compiler=$1; shift
out_dir=$1; shift

echo "Running $compiler on $revision..."

script_dir=$(dirname $(readlink -f $0))

checkout()
{
    if [ -d openssl ]; then
        return
    fi

    git clone https://github.com/openssl/openssl.git
    pushd openssl
    git checkout $revision
    git log -n1
    popd
}

get_llvm_nightly()
{
    if [ -d llvm ]; then
        return
    fi

    # need https://github.com/llvm/llvm-project/pull/81849
    # missed llvm-18, so nightly is required waiting for llvm-19
    wget https://woastorage.blob.core.windows.net/packages/llvm/llvm_main.zip
    unzip llvm_main.zip
    unzip LLVM-*.zip
    rm *.zip
    mv LLVM-* llvm
}

# disable sm2, issue with build system for clang-cl
clang_disabled="no-sm2"

setup()
{
    pushd openssl
    case $compiler in
        "cl")
            perl Configure VC-WIN64-ARM
            ;;
        "full-clang-cl")
            if [ "$revision" != "master" ]
            then
                die "Clang is only supported on master"
            else
                perl Configure VC-CLANG-WIN64-CLANGASM-ARM $clang_disabled
            fi
            ;;
        "clang-cl-as")
            if [ "$revision" != "master" ]
            then
                die "Clang is only supported on master"
            else
                perl Configure VC-WIN64-CLANGASM-ARM $clang_disabled
            fi
            ;;
    esac

    popd
}

build()
{
    pushd openssl
    nmake
    popd
}

test()
{
    pushd openssl
    nmake test || true
    popd
}

get_llvm_nightly
export PATH=$(pwd)/llvm/bin:$PATH
which clang-cl.exe
clang-cl.exe --version

checkout
setup
build
test
