import argparse
import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

DEFAULT_REVISION = "master"
DEFAULT_COMPILER = "cl"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def __init__(self, revision, compiler):
        self.revision = revision
        self.compiler = compiler

    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="openssl",
            id="openssl",
            pretty_name="OpenSSL",
            version=self.revision,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, self.revision, self.compiler, cygwin_path(out_dir))


parser = argparse.ArgumentParser(description="Build openssl")
parser.add_argument("--revision", help="Revision to build", default=DEFAULT_REVISION)
parser.add_argument("--compiler", help="Compiler to use", default=DEFAULT_COMPILER)
args = parser.parse_args()
PackageBuilder(Recipe(args.revision, args.compiler)).make()
